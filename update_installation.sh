#/usr/bin/env sh

set -e

cd dwm-6.2
sudo cp config.def.h config.h
sudo make clean install
